/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.PipelineRegister;
import baseclasses.PipelineStageBase;
import baseclasses.CpuCore;
import examples.MultiStageFunctionalUnit;
import java.util.Set;
import tools.InstructionSequence;
import utilitytypes.ClockedIntArray;
import utilitytypes.IClocked;
import utilitytypes.IGlobals;
import utilitytypes.IPipeReg;
import utilitytypes.IPipeStage;
import utilitytypes.IProperties;
import static utilitytypes.IProperties.*;
import utilitytypes.IRegFile;
import utilitytypes.Logger;
import voidtypes.VoidRegister;

/**
 * This is an example of a class that builds a specific CPU simulator out of
 * pipeline stages and pipeline registers.
 * 
 * @author 
 */
public class MyCpuCore extends CpuCore {
    static final String[] producer_props = {RESULT_VALUE};
        
    /**
     * Method that initializes the CpuCore.
     */
    @Override
    public void initProperties() {
        // Instantiate the CPU core's property container that we call "Globals".
        properties = new GlobalData();
        IGlobals globals = (GlobalData)getCore().getGlobals();
        int rat[] = globals.getPropertyIntArray("rat");
        for(int i=0;i<32;i++)
        {
        	rat[i]=i;
        }
        IRegFile regfile = globals.getRegisterFile();
        regfile.markPhysical();
        for(int i =0;i<32;i++) {
        		regfile.markUsed(i,true);
        }
    }
    
        private void freePhysRegs() {
        IGlobals globals = getGlobals();
        IRegFile regfile = globals.getRegisterFile();
        boolean printed = false;
        for (int i=0; i<256; i++) {
            if (regfile.isUsed(i)) {
                if (!regfile.isInvalid(i) && regfile.isRenamed(i)) {
                    regfile.markUsed(i, false);
                    if (!printed) {
                        Logger.out.print("# Freeing:");
                        printed = true;
                    }
                    Logger.out.print(" P" + i);
                }
            }
        }
        if (printed) Logger.out.println();
    }

// Set all RAT entries to -1, mapping architectural register numbers to the ARF.
// Set all ARF entries as USED and VALID
    
    
    public void loadProgram(InstructionSequence program) {
        getGlobals().loadProgram(program);
    }
    
    public void runProgram() {
        properties.setProperty(IProperties.CPU_RUN_STATE, IProperties.RUN_STATE_RUNNING);
        while (properties.getPropertyInteger(IProperties.CPU_RUN_STATE) != IProperties.RUN_STATE_HALTED) {
            Logger.out.println("## Cycle number: " + cycle_number);
            Logger.out.println("# State: " + getGlobals().getPropertyInteger(IProperties.CPU_RUN_STATE));
            IClocked.advanceClockAll();
        }
    }

    @Override
    public void createPipelineRegisters() {
        createPipeReg("FetchToDecode");
        //createPipeReg("DecodeToExecute");
        //createPipeReg("DecodeToMemory");
       // createPipeReg("DecodeToMSFU");
       // createPipeReg("ExecuteToWriteback");
        //createPipeReg("MemoryToWriteback");
        createPipeReg("DecodeToIQ");
        createPipeReg("IQToExecute");
        createPipeReg("IQTomem");
        createPipeReg("IQTodiv");
        createPipeReg("IQTof_div");
        createPipeReg("IQTomul");
        createPipeReg("IQTof_asc");
        createPipeReg("IQTof_mul");
        createPipeReg("ExecuteToWriteback");
        createPipeReg("IDivToWriteback");
        createPipeReg("FDivToWriteback");
    }

    @Override
    public void createPipelineStages() {
        addPipeStage(new AllMyStages.Fetch(this));
        addPipeStage(new AllMyStages.Decode(this));
        addPipeStage(new AllMyStages.Execute(this));
       // addPipeStage(new AllMyStages.Writeback(this));
        addPipeStage(new Float_Div(this));
        addPipeStage(new Int_Div(this));
        addPipeStage(new Issue_Queue(this));
    }

    @Override
    public void createChildModules() {
        // MSFU is an example multistage functional unit.  Use this as a
        // basis for FMul, IMul, and FAddSub functional units.
        addChildUnit(new Int_Mul(this));
        addChildUnit(new FloatAdd_Sub_Cmp(this));
        addChildUnit(new Float_Mul(this));
        addChildUnit(new MemUnit(this));        

    }

    @Override
    public void createConnections() {
        // Connect pipeline elements by name.  Notice that 
        // Decode has multiple outputs, able to send to Memory, Execute,
        // or any other compute stages or functional units.
        // Writeback also has multiple inputs, able to receive from 
        // any of the compute units.
        // NOTE: Memory no longer connects to Execute.  It is now a fully 
        // independent functional unit, parallel to Execute.
        
        // Connect two stages through a pipelin register
        connect("Fetch", "FetchToDecode", "Decode");
        
        // Decode has multiple output registers, connecting to different
        // execute units.  
        // "MSFU" is an example multistage functional unit.  Those that
        // follow the convention of having a single input stage and single
        // output register can be connected simply my naming the functional
        // unit.  The input to MSFU is really called "MSFU.in".
       connect("Decode", "DecodeToIQ", "IQ");
        
        //IssueQ to FU
        connect("IQ", "IQToExecute", "Execute");
        connect("IQ", "IQTomem", "mem");
        connect("IQ", "IQTodiv", "div");
        connect("IQ", "IQTof_div", "f_div");
        connect("IQ", "IQTomul", "mul");
        connect("IQ", "IQTof_asc", "f_asc");
        connect("IQ", "IQTof_mul", "f_mul");
              
        // Writeback has multiple input connections from different execute
        // units.  The output from MSFU is really called "MSFU.Delay.out",
        // which was aliased to "MSFU.out" so that it would be automatically
        // identified as an output from MSFU.
        connect("Execute","ExecuteToWriteback", "Writeback");
        connect("div", "IDivToWriteback", "Writeback");
        connect("f_div", "FDivToWriteback", "Writeback");
        connect("mul",  "Writeback");
        connect("f_asc", "Writeback");
        connect("f_mul", "Writeback");
        connect("mem","Writeback");
    }

    @Override
    public void specifyForwardingSources() {
        addForwardingSource("ExecuteToWriteback");
        //addForwardingSource("MemoryToWriteback");
        addForwardingSource("ExecuteToWriteback");
        addForwardingSource("mem.out");
        addForwardingSource("IDivToWriteback");
        addForwardingSource("FDivToWriteback");
        addForwardingSource("mul.out");
        addForwardingSource("f_asc.out");
        addForwardingSource("f_mul.out");
        // MSFU.specifyForwardingSources is where this forwarding source is added
        // addForwardingSource("MSFU.out");
    }

    @Override
    public void specifyForwardingTargets() {
        // Not really used for anything yet
        
    }

    @Override
    public IPipeStage getFirstStage() {
        // CpuCore will sort stages into an optimal ordering.  This provides
        // the starting point.
        return getPipeStage("Fetch");
    }
    
    public MyCpuCore() {
        super(null, "core");
        initModule();
        printHierarchy();
        Logger.out.println("");
    }
}
